package dk.cs.dwebtek;

import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaderJDOMFactory;
import org.jdom2.input.sax.XMLReaderXSDFactory;
import org.jdom2.output.XMLOutputter;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CloudService {

    private static final Namespace NS = Namespace.getNamespace("http://www.cs.au.dk/dWebTek/2014");
    private static final String SHOP_KEY = "E7CBBDB41440407FD33DD67D";
    private static final String BASE_URL = "http://webtek.cs.au.dk/cloud";

    public OperationResult<LoginResponse> login(String name, String pw) {
        int response = 0;
        try {
            Element login = new Element("login", NS);
            login.addContent(new Element("customerName", NS).addContent(name));

            if (pw.length() < 21 && pw.length() > 2) {
                login.addContent(new Element("customerPass", NS).addContent(pw));
            } else {
                return OperationResult.Fail("Password not between 3 and 20 characters: " + pw);
            }
            XMLOutputter xmlout = new XMLOutputter();
            String itemNameXML = xmlout.outputString(login);

            HttpURLConnection conn = comm("/login", "POST", true);
            if (conn == null) return OperationResult.Fail("Something went wrong");
            conn.connect();

            OutputStream temp = conn.getOutputStream();
            temp.write(itemNameXML.getBytes("UTF-8"));
            temp.flush();

            response = conn.getResponseCode();

            if (response == 200) {
                Document document = new SAXBuilder().build(conn.getInputStream());

                Element root = document.getRootElement();
                conn.disconnect();
                return OperationResult.Success(new LoginResponse(
                        root.getChildText("customerID", NS), root.getChildText("customerName", NS)
                ));
            }
            conn.disconnect();

            return OperationResult.Fail("" + response);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JDOMException e) {
            e.printStackTrace();
        }
        return OperationResult.Fail("");
    }

    public OperationResult<RegisterResponse> register(String name, String pw) {
        int response = 0;
        try {
            Element register = new Element("createCustomer", NS);
            register.addContent(new Element("shopKey", NS).addContent(SHOP_KEY));
            register.addContent(new Element("customerName", NS).addContent(name));

            if (pw.length() < 21 && pw.length() > 2) {
                register.addContent(new Element("customerPass", NS).addContent(pw));
            } else {
                return OperationResult.Fail("Password not between 3 and 20 characters: " + pw);
            }
            XMLOutputter xmlout = new XMLOutputter();
            String itemNameXML = xmlout.outputString(register);

            HttpURLConnection conn = comm("/createCustomer", "POST", true);
            if (conn == null) return OperationResult.Fail("Something went wrong");
            conn.connect();

            OutputStream temp = conn.getOutputStream();
            temp.write(itemNameXML.getBytes("UTF-8"));
            temp.flush();

            response = conn.getResponseCode();

            if (response == 200) {
                Document document = new SAXBuilder().build(conn.getInputStream());

                Element root = document.getRootElement();
                conn.disconnect();
                return OperationResult.Success(new RegisterResponse(
                        root.getChildText("customerID", NS), root.getChildText("usernameTaken", NS)
                ));
            }


            conn.disconnect();

            return OperationResult.Fail("" + response);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JDOMException e) {
            e.printStackTrace();
        }
        return OperationResult.Fail("");
    }

    public OperationResult<String> deleteItem(int itemID) {

        Element deleteItem = new Element("deleteItem", NS);
        deleteItem.addContent(new Element("shopKey", NS).addContent(SHOP_KEY));
        deleteItem.addContent(new Element("itemID", NS).addContent("" + itemID));

        int response = 0;

        try {

            HttpURLConnection conn = comm("/deleteItem", "POST", true);
            if (conn == null) return OperationResult.Fail("Something went wrong");

            conn.connect();

            XMLOutputter xmlout = new XMLOutputter();
            String deleteItemXML = xmlout.outputString(deleteItem);
            System.out.println(deleteItemXML);

            OutputStream temp = conn.getOutputStream();
            temp.write(deleteItemXML.getBytes("UTF-8"));
            temp.flush();

            response = conn.getResponseCode();
            System.out.println("Response code:" + response);
            if (response == 200) {
                return OperationResult.Success();
            } else {
                return OperationResult.Fail("Error code: " + response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return OperationResult.Fail("Failed to delete item");
    }

    /**
     * Builds a modifyItem xml-element
     *
     * @param itemID
     * @param itemName
     * @param itemPrice
     * @param itemURL
     * @param itemDescription
     * @return A dk.cs.au.dwebtek.dk.cs.dwebtek.OperationResult with the XML as a string in the result, if the XML is well-formed.
     */

    public OperationResult<String> modifyItem(int itemID, String itemName, int itemPrice, String itemURL, String itemDescription) {
        Element modifyItem = new Element("modifyItem", NS);
        modifyItem.addContent(new Element("shopKey", NS).addContent(SHOP_KEY));
        modifyItem.addContent(new Element("itemID", NS).addContent(Integer.toString(itemID)));
        modifyItem.addContent(new Element("itemName", NS).addContent(itemName));
        modifyItem.addContent(new Element("itemPrice", NS).addContent(Integer.toString(itemPrice)));
        modifyItem.addContent(new Element("itemURL", NS).addContent(itemURL));


        // itemDescription is a string, possible with XML content. We have to transform it.
        //
        OperationResult<Element> itemDescRes = convertItemDescription(itemDescription);
        if (!itemDescRes.isSuccess()) return OperationResult.Fail("Malformed XML in item description");
        modifyItem.addContent(new Element("itemDescription", NS).addContent(itemDescRes.getResult()));

        OperationResult<Object> validated = validate(new Document(modifyItem));

        if (validated.isSuccess()) {
            // HINT: You can get the XML as a string from a Document by XMLOutputter
            XMLOutputter xmlout = new XMLOutputter();
            String res = xmlout.outputString(modifyItem);
            int response = 0;

            try {
                HttpURLConnection conn = comm("/modifyItem", "POST", true);
                if (conn == null) return OperationResult.Fail("Something went wrong");

                conn.connect();

                OutputStream temp = conn.getOutputStream();
                temp.write(res.getBytes("UTF-8"));
                temp.flush();
                response = conn.getResponseCode();
                if (response == 200) {
                    return OperationResult.Success();
                } else {
                    return OperationResult.Fail("Error code: " + response);
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return OperationResult.Success(res);
        } else {
            return OperationResult.Fail(validated.getMessage());
        }
    }

    /**
     * Converts a string to an element.
     *
     * @param content the body of the document-element
     * @return A dk.cs.au.dwebtek.dk.cs.dwebtek.OperationResult where the result is an element if the xml is well-formed
     */
    private OperationResult<Element> convertItemDescription(String content) {
        content = "<document>" + content + "</document>";
        try {
            SAXBuilder b = new SAXBuilder();
            Document d = b.build(new ByteArrayInputStream(content.getBytes("UTF-8")));
            Element e = d.getRootElement();
            e.detach();
            setNamespace(e);

            return OperationResult.Success(e);

        } catch (JDOMException e) {
            e.printStackTrace();
            return OperationResult.Fail("XML not well formed");
        } catch (IOException e) {
            e.printStackTrace();
            return OperationResult.Fail("XML not well formed");
        }

        // HINT: surround the content with "<document>" and "</document>" before parsing
    }


    /**
     * Sets the namespace on the element - what about the children??
     *
     * @param child the xml-element to have set the namespace
     */
    private void setNamespace(Element child) {
        List<Element> children = child.getChildren();
        for (Element ch : children) {
            setNamespace(ch);
        }
        child.setNamespace(NS);
    }

    /**
     * Validates the document according to the schema cloud.xsd
     *
     * @param doc
     * @return dk.cs.au.dwebtek.dk.cs.dwebtek.OperationResult with information about success or failure
     */
    private OperationResult<Object> validate(Document doc) {

        URL url = getClass().getClassLoader().getResource("resources/cloud.xsd");
        XMLReaderJDOMFactory factory;

        try {
            factory = new XMLReaderXSDFactory(url);
        } catch (JDOMException e) {
            return OperationResult.Fail("Could not find schema");
        }

        String xml = new XMLOutputter().outputString(doc);
        SAXBuilder builder = new SAXBuilder(factory);
        try {
            builder.build(new StringReader(xml));
        } catch (JDOMException e) {
            return OperationResult.Fail("Xml is not valid: " + e.getMessage());
        } catch (IOException e) {
            return OperationResult.Fail("YIKES: " + e.getMessage());
        }

        return OperationResult.Success(true);
    }

    public OperationResult<Integer> createItem(String itemName) {
        int response = 0;
        try {
            Element createItem = new Element("createItem", NS);
            createItem.addContent(new Element("shopKey", NS).addContent(SHOP_KEY));
            createItem.addContent(new Element("itemName", NS).addContent(itemName));

            XMLOutputter xmlout = new XMLOutputter();
            String itemNameXML = xmlout.outputString(createItem);

            HttpURLConnection conn = comm("/createItem", "POST", true);
            if (conn == null) return OperationResult.Fail("Something went wrong");
            conn.connect();

            OutputStream temp = conn.getOutputStream();
            temp.write(itemNameXML.getBytes("UTF-8"));
            temp.flush();

            if (conn.getResponseCode() == 200) {
                Document document = new SAXBuilder().build(conn.getInputStream());
                int itemID = Integer.parseInt(document.getRootElement().getText());
                conn.disconnect();
                return OperationResult.Success(itemID);
            }
            response = conn.getResponseCode();

            conn.disconnect();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JDOMException e) {
            e.printStackTrace();
        }
        return OperationResult.Fail("Could not create Item: " + response);
    }

    public OperationResult<Boolean> updateStock(int newStock, int itemID) {
        int response = 0;
        try {
            Element adjustItemStock = new Element("adjustItemStock", NS);
            adjustItemStock.addContent(new Element("shopKey", NS).addContent(SHOP_KEY));
            adjustItemStock.addContent(new Element("itemID", NS).addContent("" + itemID));
            adjustItemStock.addContent(new Element("adjustment", NS).addContent("" + newStock));

            XMLOutputter xmlout = new XMLOutputter();
            String itemNameXML = xmlout.outputString(adjustItemStock);

            HttpURLConnection conn = comm("/adjustItemStock", "POST", true);
            if (conn == null) return OperationResult.Fail("Something went wrong");
            conn.connect();

            OutputStream temp = conn.getOutputStream();
            temp.write(itemNameXML.getBytes("UTF-8"));
            temp.flush();

            if (conn.getResponseCode() == 200) {
                conn.disconnect();
                return OperationResult.Success();
            }
            response = conn.getResponseCode();

            conn.disconnect();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return OperationResult.Fail("Could not update Stock: " + response);
    }

    public Item getItem(int itemID, int shopID) {
        for (Item i : listItemFromShop(shopID).getResult()) {
            if (i.getItemID() == itemID) return i;
        }
        return null;
    }

    public OperationResult<List<Item>> listItems() {
        return listItemFromShop(455);
    }

    public OperationResult<List<Item>> listItemFromShop(int shopID) {
        List deletedItems = listDeletedItems(shopID).getResult();
        List<Item> list = new ArrayList<>();

        try {
            HttpURLConnection conn = comm("/listItems?shopID=" + shopID, "GET", false);
            if (conn == null) return OperationResult.Fail("Something went wrong");

            if (conn.getResponseCode() == 200) {
                Document document = new SAXBuilder().build(conn.getInputStream());
                List<Element> elementList = document.getRootElement().getChildren();
                conn.disconnect();
                int cnt = 0;
                for (Element el : elementList) {
                    if (!(deletedItems.contains(Integer.parseInt(el.getChild("itemID", NS).getValue())))) {
                        Element temp = el.getChild("itemDescription", NS).getChild("document", NS);
                        temp.removeNamespaceDeclaration(NS);
                        String itdesc = new XMLOutputter().outputString(temp);
                        list.add(new Item(
                                Integer.parseInt(el.getChild("itemID", NS).getValue()),
                                el.getChild("itemName", NS).getValue(),
                                el.getChild("itemURL", NS).getValue(),
                                Integer.parseInt(el.getChild("itemPrice", NS).getValue()),
                                Integer.parseInt(el.getChild("itemStock", NS).getValue()),
                                itdesc));
                        list.get(cnt).setRenderedItemDescription(el.getChild("itemDescription", NS));
                        cnt++;
                    }
                }
                return OperationResult.Success(list);
            }

        } catch (UnknownHostException e) {
            return OperationResult.Fail("Unknown Host Exception: Check your internet access");
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return OperationResult.Fail("Something went wrong");
    }

    public OperationResult<List<Integer>> listDeletedItems(int shopID) {
        List<Integer> list = new ArrayList<>();

        try {
            HttpURLConnection conn = comm("/listDeletedItemIDs?shopID=" + shopID, "GET", false);
            if (conn == null) return OperationResult.Fail("Something went wrong");

            if (conn.getResponseCode() == 200) {
                Document document = new SAXBuilder().build(conn.getInputStream());
                List<Element> elementList = document.getRootElement().getChildren();
                conn.disconnect();
                for (Element el : elementList) {
                    list.add(
                            Integer.parseInt(
                                    el.getText()
                            )
                    );
                }
                return OperationResult.Success(list);
            }

        } catch (UnknownHostException e) {
            return OperationResult.Fail("Unknown Host Exception: Check your internet access");
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return OperationResult.Fail("Something went wrong");
    }

    private HttpURLConnection comm(String url, String method, boolean output) {
        try {
            URL adress = new URL(BASE_URL + url);
            HttpURLConnection conn = (HttpURLConnection) adress.openConnection();
            conn.setDoOutput(output);
            conn.setRequestMethod(method);
            return conn;
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public OperationResult<String> buyItems(Map cart, int custID) {
        int response = 0;
        boolean res = true;

        int shopID = 455;
        for (Object i : cart.keySet()) {
            Item item = getItem((int) i, shopID);
            String shopkey = SHOP_KEY;
            if (item == null) {
                item = getItem((int) i, 453);
            }
            res = res && buyItemsServer(item.getItemID(), (int) cart.get(i), shopkey, custID).isSuccess();

        }
        if (res) {
            return OperationResult.Success("" + response);
        } else {
            return OperationResult.Fail("One or more items were not bought.");
        }

    }

    public OperationResult<SalesResponse> buyItemsServer(int itemID, int saleAmount, String shopID, int custID) {
        try {
            Element sellItems = new Element("sellItems", NS);
            sellItems.addContent(new Element("shopKey", NS).addContent("" + shopID));
            sellItems.addContent(new Element("itemID", NS).addContent("" + itemID));
            sellItems.addContent(new Element("customerID", NS).addContent("" + custID));
            sellItems.addContent(new Element("saleAmount", NS).addContent("" + saleAmount));

            HttpURLConnection conn = comm("/sellItems", "POST", true);
            if (conn == null) return OperationResult.Fail("Something went wrong");
            conn.connect();
            OutputStream outputStream = conn.getOutputStream();

            XMLOutputter xmlout = new XMLOutputter();
            String itemNameXML = xmlout.outputString(sellItems);
            System.out.println(itemNameXML);

            outputStream.write(itemNameXML.getBytes("UTF-8"));
            outputStream.flush();

            if (conn.getResponseCode() == 200) {
                System.out.println("conn.getResponseMessage() = " + conn.getResponseMessage());
                Document document = new SAXBuilder().build(conn.getInputStream());
                System.out.println(document.getRootElement().getText());
                conn.disconnect();
                return OperationResult.Success();
            }
            conn.disconnect();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JDOMException e) {
            e.printStackTrace();
        }
        return OperationResult.Fail("Could not update buy item: " + itemID);
    }
}

