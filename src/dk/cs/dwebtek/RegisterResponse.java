package dk.cs.dwebtek;

/**
 * Created by Johannes on 05-03-2017.
 */
public class RegisterResponse {
    private String customerID;
    private String userNameTaken;

    public RegisterResponse(String customerID, String userNameTaken) {
        this.customerID = customerID;
        this.userNameTaken = userNameTaken;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getUserNameTaken() {
        return userNameTaken;
    }

    public void setUserNameTaken(String userNameTaken) {
        this.userNameTaken = userNameTaken;
    }
}
