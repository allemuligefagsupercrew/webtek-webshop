package dk.cs.dwebtek;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.*;

@Path("shop")
public class ShopService {
    /**
     * Our Servlet session. We will need this for the shopping basket
     */
    HttpSession session;
    CloudService cs;

    public ShopService(@Context HttpServletRequest servletRequest)
    {
        session = servletRequest.getSession();
        if(session.getAttribute("cart") == null){
            session.setAttribute("cart", new HashMap<Integer,Integer>());
        }
        cs = new CloudService();
    }


    @POST
    @Path("logOut")
    public void logOut(){
        session.setAttribute("login",null);
    }

    @GET
    @Path("loggedIn")
    public String loggedIn(){

        if(session.getAttribute("login")!= null){
            return  ((dk.cs.dwebtek.LoginResponse)session.getAttribute("login")).getCustomerName();
        }else{
            return "Not Logged In";
        }

    }

    @POST
    @Path("register")
    public String register(@FormParam("userName") String userName, @FormParam("password") String pw) {
        OperationResult<RegisterResponse> temp = cs.register(userName, pw);
        if (temp.isSuccess()) {
            return login(userName,pw);
        }
        return "Could not register: " + temp.getMessage();
    }

    @POST
    @Path("login")
    public String login(@FormParam("userName") String userName, @FormParam("password") String pw){
        System.out.println(userName);
        OperationResult<LoginResponse> temp = cs.login(userName, pw);
        if(temp.isSuccess()){
            session.setAttribute("login",temp.getResult());
            return "hi " + temp.getResult().getCustomerName();
        }
        return "Could not login: " + temp.getMessage();
    }

    @GET
    @Path("items")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Item> getItems() {

        //You should get the items from the cloud server.
        //In the template we just construct some simple data as an array of objects
        // Here we have the JSON automated automatically by having models.
        return cs.listItems().getResult();
    }

    @GET
    @Path("dankify")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Item> dankify() {
        //You should get the items from the cloud server.
        //In the template we just construct some simple data as an array of objects
        // Here we have the JSON automated automatically by having models.
        return cs.listItemFromShop(453).getResult();
    }

    @POST
    @Path("addToCart")
    public void addToCart(@FormParam("id") String itemID, @FormParam("body") String bodyName){
        int shopID;
        if(bodyName.equals("dankifyBody")){
            shopID = 453;
        }else{
            shopID = 455;
        }
        int id = Integer.parseInt(itemID);

        if(session.getAttribute("cart") != null) {
            Map map = (Map) session.getAttribute("cart");
            Item item = cs.getItem(id, shopID);
            if(map.containsKey(id) && (int)map.get(id)< item.getItemStock()){
                map.replace(id, map.get(id), (int)map.get(id) + 1);
            }else if(item.getItemStock()>0 && !map.containsKey(id)){
                map.put(id, 1);
            }
        }
    }

    @GET
    @Path("cartSize")
    public String getCartSize(){
        System.out.println(session.getAttribute("cart"));
        if(session.getAttribute("cart") != null) {
            Map cart = (Map) session.getAttribute("cart");
            int count = 0;
            for(Object i : cart.keySet()){
                count += (int)cart.get(i);
            }
            return "" + count;

        }
        System.out.println(session.getAttribute("cart"));
        return ""+0;
    }

    @POST
    @Path("purchase")
    public String purchase(){
                return cs.buyItems(
                (Map)session.getAttribute("cart"),
                Integer.parseInt(((dk.cs.dwebtek.LoginResponse)session.getAttribute("login")).getCustomerID())).getResult();
    }

    @POST
    @Path("emptyCart")
    public void emptyCart(){
        session.setAttribute("cart", new HashMap<Integer,Integer>());
    }

    @GET
    @Path("cartItems")
    public String getCartItems(){
        String innerHtml = "<table>";

        if(session.getAttribute("cart") != null) {
            Map cart = (Map) session.getAttribute("cart");
            for(Object i : cart.keySet()){
                innerHtml += "<tr><td style=\"border: none;!important;\">" + cs.getItem((int)i, 455).getItemName() + "</td><td style=\"border: none;!important;\">" + cart.get(i) + "</td> \n";
            }
        }
        else{
            innerHtml = "Your Cart is Empty.";
        }
        innerHtml += "</table>";
        return innerHtml;
    }

}
