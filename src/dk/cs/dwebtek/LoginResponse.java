package dk.cs.dwebtek;

/**
 * Created by Johannes on 05-03-2017.
 */
public class LoginResponse {
    private String customerID;
    private String customerName;

    public LoginResponse(String customerID, String customerName) {
        this.customerID = customerID;
        this.customerName = customerName;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Override
    public String toString() {
        return customerID + customerName;
    }
}
