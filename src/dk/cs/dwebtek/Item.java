package dk.cs.dwebtek;

import org.jdom2.Content;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.XMLOutputter;

import java.util.List;

public class Item {
    private int itemID, itemPrice, itemStock;
    private String itemName, itemURL, itemDescription;
    private String itemIDStr, itemPriceStr, itemStockStr, renderedItemDescription;

    public void setRenderedItemDescription(Element el){
        renderedItemDescription = itemDescRender(el);
    }

    public String getRenderedItemDescription() {
        return renderedItemDescription;
    }

    public String getItemDescription() {
        String newItemDescription = itemDescription;
        String bigNS = "<w:document xmlns:w=\"http://www.cs.au.dk/dWebTek/2014\">";
        String bigNS2 = "<document xmlns:w=\"http://www.cs.au.dk/dWebTek/2014\" />";
        String endDoc = "</document>";
        newItemDescription = newItemDescription.replace(bigNS,"");
        newItemDescription = newItemDescription.replace(bigNS2,"");
        newItemDescription = newItemDescription.replaceAll("w:","");
        newItemDescription = newItemDescription.replaceAll(endDoc,"");
        return newItemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemName() {

        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemURL() {

        return itemURL;
    }

    public void setItemURL(String itemURL) {
        this.itemURL = itemURL;
    }

    public int getItemStock() {

        return itemStock;
    }

    public void setItemStock(int itemStock) {
        this.itemStock = itemStock;
    }

    public int getItemPrice() {

        return itemPrice;
    }

    public void setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getItemID() {

        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public String getItemIDStr() {
        return "" + itemID;
    }

    public void setItemIDStr(String itemIDStr) {
        this.itemID = Integer.parseInt(itemIDStr);
    }

    public String getItemPriceStr() {
        return "" + itemPrice;
    }

    public void setItemPriceStr(String itemPriceStr) {
        this.itemPrice = Integer.parseInt(itemPriceStr);
    }

    public String getItemStockStr() {
        return "" + itemStock;
    }

    public void setItemStockStr(String itemStockStr) {
        System.out.println("Setting item stock");
        this.itemStock = Integer.parseInt(itemStockStr);
    }
    

    public Item(int ID, String Name, String url, int price, int stock, String itemDescription) {
        this.itemID = ID;
        this.itemName = Name;
        this.itemURL = url;
        this.itemPrice = price;
        this.itemStock = stock;
        this.itemDescription = itemDescription;
    }

    @Override
    public String toString() {
        return "" + itemName + "\n" +
                "   " + "Id: " + this.itemID + "\n" +
                "   " + "Name: " + this.itemName + "\n" +
                "   " + "URL: " + this.itemURL + "\n" +
                "   " + "Price: " + this.itemPrice + "\n" +
                "   " + "Stock: " + this.itemStock + "\n" +
                "   " + "Description: \n" + "   " + this.itemDescription + "\n \n";
    }

    public String updateStock() {
        OperationResult<Boolean> temp = new CloudService().updateStock(itemStock,itemID);
        if (temp.isSuccess()){
            return "updated";
        }
        System.out.println(temp.getMessage());
        return "failed to update stock";
    }
    private String itemDescRender(Element itemDesc) {
        itemDesc.setNamespace(Namespace.NO_NAMESPACE);
        List<Content> contents = itemDesc.getContent();

        for (Content c : contents) {
            if (c instanceof Element) {
                Element e = (Element) c;
                if (e.getName().equals("bold")) {
                    e.setName("b");
                } else if (e.getName().equals("italics")) {
                    e.setName("i");
                } else if (e.getName().equals("list")) {
                    e.setName("ul");
                } else if (e.getName().equals("item")) {
                    e.setName("li");
                }else if(e.getName().equals("document")){
                    e.setName("span");
                }
                e.setNamespace(Namespace.NO_NAMESPACE);
                itemDescRender(e);
            }
        }
        return new XMLOutputter().outputString(itemDesc);
    }
}
