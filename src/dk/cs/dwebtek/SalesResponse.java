package dk.cs.dwebtek;

/**
 * Created by Johannes on 07-03-2017.
 */
public class SalesResponse {
    String ok;
    String itemSoldOut;
    String error;

    public SalesResponse(String ok, String itemSoldOut, String error) {
        this.ok = ok;
        this.itemSoldOut = itemSoldOut;
        this.error = error;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getItemSoldOut() {
        return itemSoldOut;
    }

    public void setItemSoldOut(String itemSoldOut) {
        this.itemSoldOut = itemSoldOut;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
