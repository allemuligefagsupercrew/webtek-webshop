var cartSum = 0;
var dankified = false;
var globalLoggedIn = false;
//Run this function when we have loaded the HTML document
window.onload = function () {
    //This code is called when the body element has been loaded and the application starts
    dankified = false;
    var cart = document.getElementById("cart");
    var cart2 = document.getElementById("cart2")
    cart.innerHTML = cartSum;
    cart2.innerHTML = cartSum;

    //Request items from the server. The server expects no request body, so we set it to null
    sendRequest("GET", "rest/shop/items", null, function (itemsText) {
        //This code is called when the server has sent its data
        var items = JSON.parse(itemsText);
        addItemsToTable(items, "itemtablebody");
        loggedIn();
    });
    /*
    //Register an event listener for button clicks
    updateButton = document.getElementById("update");
    addEventListener(updateButton, "click", function () {
        //Same as above, get the items from the server
        sendRequest("GET", "rest/shop/items", null, function (itemsText) {
            //This code is called when the server has sent its data
            var items = JSON.parse(itemsText);
            addItemsToTable(items, "itemtablebody");
        });
    }); */


    var logoutButton = document.getElementById("logOutBtn");
    addEventListener(logoutButton, "click", function () {
        console.log("logging out");
        //Same as above, get the items from the server
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'rest/shop/logOut', true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.onload = function () {
            console.log("logger ud");
            //This code is called when the server has sent its data
            document.getElementById("logOutBtn").style.display = "none";
            document.getElementById("login-Div").style.display = "inline";
            document.getElementById("loggedIn").innerHTML = "";
            globalLoggedIn = false;

        };
        xhr.send();
    });
    var purchaseBtn = document.getElementById("buy");
    var purchaseBtn2 = document.getElementById("buy2");


    addEventListener(purchaseBtn, "click", buy);
    addEventListener(purchaseBtn2, "click", buy);


};


function buy() {

    if (globalLoggedIn == true) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'rest/shop/purchase', true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.onload = function () {
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'rest/shop/emptyCart', true);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.onload = function () {
                updateCart(0);
            };
            xhr.send();

        };
        xhr.send();


    }else{
        window.alert("Du skal logge ind for at foretage et køb");
    }
}

function updateItems(){
    var items = "";
    console.log("Updating Items");
    //Same as above, get the items from the server
    sendRequest("GET", "rest/shop/items", null, function (itemsText) {
        //This code is called when the server has sent its data
        items = JSON.parse(itemsText);
        addItemsToTable(items, "itemtablebody");
    });
    addItemsToTable(items, "itemtablebody");
}

function loggedIn() {
    sendRequest("GET", "rest/shop/loggedIn", null, function (res) {
        var loggedIn = document.getElementById("loggedIn");
        if(res == "Not Logged In"){
            loggedIn.textContent = "";
            globalLoggedIn = false;
        }else{
            loggedIn.innerHTML = "<p>Welcome " + res + "</p>";
            document.getElementById("login-Div").style.display = "none";
            document.getElementById("logOutBtn").style.display = "inline";
            globalLoggedIn = true;
        }

    });
}

document.getElementById("loginButton").onclick = function () {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'rest/shop/login', true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
        if(this.responseText == "Could not login: 403"){
            window.alert("Could not log you in. Check username and password again.")
        }
        console.log(this.responseText);
        loggedIn();
    };
    console.log(document.getElementById("loginUsername").value);
    console.log(document.getElementById("loginPassword").value);
    xhr.send('&userName=' + document.getElementById("loginUsername").value + '&password=' + document.getElementById("loginPassword").value);
};

document.getElementById("registerButton").onclick = function () {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'rest/shop/register', true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
        console.log(this.responseText);
        loggedIn();
    };
    xhr.send('&userName=' + document.getElementById("loginUsername").value + '&password=' + document.getElementById("loginPassword").value);
};

document.getElementById("dankifyButton").onclick = function () {
    if (dankified == false) {
        dankified = true;
        //Same as above, get the items from the server
        sendRequest("GET", "rest/shop/dankify", null, function (itemsText) {
            //This code is called when the server has sent its data
            var items = JSON.parse(itemsText);
            addItemsToTable(items, "dankifyBody");
        });
        document.body.style.background = "url('//i.imgur.com/oQOp2hE.gif') no-repeat center center fixed";
        document.body.style.backgroundSize = "cover";
    } else {
        document.body.style.background = "";
        var tableBody = document.getElementById("dankifyBody");
        tableBody.remove();
        var temp = document.getElementById("table");
        var temp2 = document.createElement("tbody");
        temp2.id = "dankifyBody";

        temp.appendChild(temp2);

        dankified = false;
    }
};


function addItemsToTable(items, body) {
    //Get the table body we we can add items to it
    var tableBody = document.getElementById(body);

    //Remove all contents of the table body (if any exist)
    tableBody.innerHTML = "";

    //Loop through the items from the server
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        //Create a new line for this item
        var tr = document.createElement("tr");
        tr.setAttribute("id", item.itemName);

        var picCell = document.createElement("td");
        picCell.innerHTML = "<img src=" + item.itemURL + " alt=" + item.itemName + " picture" + " style=\"width:50px;height:50px;\">"
        tr.appendChild(picCell);


        var nameCell = document.createElement("td");
        nameCell.textContent = item.itemName;
        tr.appendChild(nameCell);

        var priceCell = document.createElement("td");
        priceCell.textContent = "$" + item.itemPrice;
        tr.appendChild(priceCell);

        var itemDescriptionCell = document.createElement("td");
        itemDescriptionCell.style.width = "20%";
        itemDescriptionCell.innerHTML = item.renderedItemDescription;
        tr.appendChild(itemDescriptionCell);

        var stockCell = document.createElement("td");
        stockCell.textContent = "Stock: " + item.itemStock;
        tr.appendChild(stockCell);

        var buyButtonCell = document.createElement("td");
        if (!(item.itemStock == 0)) {
            var buyButton = document.createElement("button");
            buyButton.id = item.itemID;
            addEventListener(buyButton, "click", function () {
                incrementCart(this.id, body);
            });
            buyButton.textContent = "Add Item";
            buyButtonCell.appendChild(buyButton);

        } else {
            buyButtonCell.innerHTML = "<p style='color: red'>SOLD OUT</p>"
        }
        tr.appendChild(buyButtonCell);

        tableBody.appendChild(tr);
    }

}


function updateCart(size) {
    var cart = document.getElementById("cart");
    cart.textContent = size;

    var cart2 = document.getElementById("cart2");
    cart2.textContent = size;

    var cartContent = document.getElementById("items");
    sendRequest("GET", "rest/shop/cartItems", null, function (itemz) {
        cartContent.innerHTML = itemz;
    });

    if(size == 0){
        updateItems();
        cartContent.innerHTML = "";
    }
}

function incrementCart(id, body) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'rest/shop/addToCart', true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
        sendRequest("GET", "rest/shop/cartSize", null, function (cartSize) {
            updateCart(cartSize);
        });
    };
    xhr.send('id=' + id + '&body=' + body);
}


/////////////////////////////////////////////////////
// Code from slides
/////////////////////////////////////////////////////

/**
 * A function that can add event listeners in any browser
 */
function addEventListener(myNode, eventType, myHandlerFunc) {
    if (myNode.addEventListener)
        myNode.addEventListener(eventType, myHandlerFunc, false);
    else
        myNode.attachEvent("on" + eventType,
            function (event) {
                myHandlerFunc.call(myNode, event);
            });
}

var http;
if (!XMLHttpRequest)
    http = new ActiveXObject("Microsoft.XMLHTTP");
else
    http = new XMLHttpRequest();

function sendRequest(httpMethod, url, body, responseHandler) {
    http.open(httpMethod, url);
    if (httpMethod == "POST") {
        http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    }
    http.onreadystatechange = function () {
        if (http.readyState == 4 && http.status == 200) {
            responseHandler(http.responseText);
        }
    };
    http.send(body);
}