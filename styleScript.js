

document.getElementById("search").addEventListener("keyup", function searchFunc() {

    // Declare variables
    var input, filter, table, tr, td, i, none, count;
    input = document.getElementById('search');
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");
    none = document.getElementById("nonefound");
    count = tr.length;

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if(td){
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
                none.style.display = "none";
                count ++;
            } else {
                tr[i].style.display = "none";
                count --;
            }

            if (count <= 0){
                none.style.display = "";
            }

        }
    }

    /*while (td.style.display == "none"){
        document.getElementById("nonefound").style.display = "";
    }*/

});

/* Set the width of the side navigation to 250px */

document.getElementById("showCart").addEventListener("click", function openNav() {
    document.getElementById("sidenav").style.width = "200px";
    setTimeout(function(){
        //wait before executing
        document.getElementById("sidediv").style.display = "";
    }, 300);
});

/* Set the width of the side navigation to 0 */
document.getElementById("close").addEventListener("click", function closeNav() {
    document.getElementById("sidenav").style.width = "0";
        document.getElementById("sidediv").style.display = "none";
});

